const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const port = process.env.PORT || 4000;
const app = express();
const itemRouter = require('./routes/api/items');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());



// mongoDB config
const uri = require('./setup/myurl').mongoURL
mongoose
    .connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connection Sucessful');
    })
    .catch( (error) => {
        console.log(error);
    })

app.get('/',(req,res) => {
    res.send('Hello from shopping app');
});

//use routes

app.use('/api',itemRouter);

app.listen(port,() => console.log(`Server is up and running on port:${port}`));