const express = require('express');
const router = express.Router();
const items = require('../../models/itemModel');


//@route GET api/items
//@desc Get All Items
//@access Public

router.get('/',(req,res) => {
    items.find()
        .sort({date: -1})
        .then(items => {
            res.json(items);
        })
        .catch(error => console.log(error));
})

//@route Post api/items
//@desc Save Items into database
//@access Public

router.post('/',(req,res) => {
    const newItem = new items({
        name : req.body.name
    });
    newItem.save()
    .then(item => {
        res.json(item);
    })
    .catch(error => console.log(error));
})


//@route Delete api/items/:id
//@desc delete Items from database
//@access Public

router.delete('/:id',(req,res) => {
    items.findById(req.params.id)
        .then(item => {
            item.remove()
                .then(() => res.json({"Item Deleted": "item deleted sucessfully"}))
                .catch(error => console.log(error));
        })
        .catch(error => res.status(404).json({"Item Not Found":"Item Not Found To Delete"}));
        
})

module.exports = router;